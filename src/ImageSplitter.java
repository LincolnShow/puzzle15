import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Arrays;

public class ImageSplitter implements Squares {
    public static BufferedImage[] split(BufferedImage image, int pieces, boolean full){
        BufferedImage[] result = new BufferedImage[full ? pieces : pieces-1];
        int factor;
        factor = PERFECT.indexOf(pieces);
        if(factor != -1){
            factor+=2;
            int elemW = image.getWidth() / factor;
            int elemH = image.getHeight() / factor;

            try {
                int pos = 0;
                for (int row = 0; row < factor; ++row) {
                    for (int column = 0; column < factor; ++column) {
                        result[pos++] = image.getSubimage(column * elemW, row * elemH, elemW, elemH);
                    }
                }
            }catch (ArrayIndexOutOfBoundsException e){}
        }
        return result;
    }
}
