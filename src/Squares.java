import java.util.*;

public interface Squares {
    static final Integer[] PERFECT_VALUES = new Integer[]{4,9,16,25,36,49,64,81,100};
    static final List<Integer> PERFECT  = new LinkedList<>(Arrays.asList(PERFECT_VALUES));
}
