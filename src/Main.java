import java.io.IOException;

/**
 * Created by Alexander on 28.10.2016.
 */
public class Main {
    public static void main(String[] args) throws IOException {
        Game game = new Game();
        game.start();
    }
}
