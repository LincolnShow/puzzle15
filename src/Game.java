import net.coobird.thumbnailator.Thumbnails;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;

public class Game implements Squares {
    static int SQUARE = 100;
    final static int HPAD = 3;
    final static int VPAD = 3;
    private GameCell[][]field;
    private GridBagLayout layout;
    private GameCell EMPTY = null;
    enum Direction{
        EAST, NORTH, WEST, SOUTH
    }

    public void start() throws IOException {
        BufferedImage face = ImageIO.read(new File("D:/wow.jpg"));
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        if(face.getWidth() >= screenSize.width || face.getHeight() >= screenSize.height){
            face = Thumbnails.of(face).size(screenSize.width-100, screenSize.height-100).asBufferedImage();
        }
        BufferedImage[] pieces = ImageSplitter.split(face, SQUARE, true);
        int factor = getFactor(SQUARE);
        field = new GameCell[factor][factor];
        JPanel mainPanel = new JPanel(new BorderLayout());
        layout = new GridBagLayout();
        mainPanel.setLayout(layout);
        JFrame frame = new JFrame("Puzzle15");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(mainPanel);
        for(int y = 0, index = 0; y < factor; ++y){
            for (int x = 0; x < factor; ++x){
                field[y][x] = new GameCell(new ImageIcon(pieces[index]), y, x);
                field[y][x].winIndex = index++;
            }
        }
        EMPTY = field[factor-1][factor-1];
        shuffleCells();
        for(GameCell[] row : field){
            for (GameCell c : row){
                mainPanel.add(c);
            }
        }
        field[factor-1][factor-1].setVisible(false);
        setUpPositions(factor);
        mainPanel.setOpaque(true);


        mainPanel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                try {
                    GameCell clickedCell = (GameCell) mainPanel.getComponentAt(e.getX(), e.getY());
                    GameCell empty = getFreeNeighbour(clickedCell);
                    if(empty != null){
                        swapFieldPos(clickedCell, empty);
                    }
                }catch (ClassCastException ex){
                }
                mainPanel.revalidate();
                if(winCheck()){
                    EMPTY.setVisible(true);
                    JOptionPane.showMessageDialog(frame, "You won!");
                }
            }
        });

        frame.pack();
        frame.setVisible(true);
        frame.setResizable(true);
    }
    private void setUpPositions(int factor){
        for (int row = 0; row < factor; ++row){
            for (int col = 0; col < factor; ++col){
                GridBagConstraints c = new GridBagConstraints();
                c.gridx = col;
                c.gridy = row;
                c.ipadx = HPAD;
                c.ipady = VPAD;
                layout.setConstraints(field[row][col], c);
            }
        }
    }
    private int getFactor(int pieces){
        int factor = PERFECT.indexOf(pieces);
        if(factor != -1) {
            factor += 2;
        }
        return factor;
    }
    private void shuffleCells() {
        GameCell[] array = new GameCell[field.length*field[0].length];

        int q = 0;
        for (GameCell[] cells : field){
            for (GameCell cell : cells){
                array[q++] = cell;
            }
        }

        int index;
        GameCell temp;
        Random random = new Random();
        for (int i = array.length - 1; i > 0; i--)
        {
            index = random.nextInt(i + 1);
            temp = array[index];
            array[index].swapPos(array[i]);
            array[index] = array[i];
            array[i] = temp;
        }

        q = 0;
        for(int y = 0; y < field.length; ++y){
            for(int x = 0; x < field[0].length; ++x){
                field[y][x] = array[q++];
            }
        }
    }
    private GameCell getFreeNeighbour(GameCell cell){
        GameCell result = null;
        Set<Direction> ways = new HashSet<>(Arrays.asList(Direction.values()));
        int x = cell.x;
        int y = cell.y;
        if(x == 0) {
            ways.remove(Direction.WEST);
        }if(x == field[0].length-1){
            ways.remove(Direction.EAST);
        }if(y == 0){
            ways.remove(Direction.NORTH);
        }if(y == field.length-1){
            ways.remove(Direction.SOUTH);
        }
        for(Direction d : ways){
            result = checkNeighbour(cell, d);
            if(result != null){
                break;
            }
        }
        return result;
    }
    private GameCell checkNeighbour(GameCell cell, Direction dir){
        GameCell result = null;
        switch (dir){
            case NORTH:
                result = !field[cell.y-1][cell.x].isVisible() ? field[cell.y-1][cell.x] : null;
                break;
            case EAST:
                result = !field[cell.y][cell.x+1].isVisible() ? field[cell.y][cell.x+1] : null;
                break;
            case SOUTH:
                result = !field[cell.y+1][cell.x].isVisible() ? field[cell.y+1][cell.x] : null;
                break;
            case WEST:
                result = !field[cell.y][cell.x-1].isVisible() ? field[cell.y][cell.x-1] : null;
                break;
        }
        return result;
    }
    private void swapFieldPos(GameCell c1, GameCell emptyCell){
        c1.swapPos(emptyCell);
        field[c1.y][c1.x] = c1;
        field[emptyCell.y][emptyCell.x] = emptyCell;

        GridBagConstraints c1c = layout.getConstraints(c1);
        GridBagConstraints c2c = layout.getConstraints(emptyCell);

        GridBagConstraints tmp = layout.getConstraints(emptyCell);
        c2c.gridx = c1c.gridx;
        c2c.gridy = c1c.gridy;
        c1c.gridx = tmp.gridx;
        c1c.gridy = tmp.gridy;

        layout.setConstraints(c1, c1c);
        layout.setConstraints(emptyCell, c2c);
        EMPTY = emptyCell;
    }
    private boolean winCheck(){
        int i = 0;
        for(GameCell[]row : field){
            for (GameCell cell : row){
                if(i != cell.winIndex){
                    return false;
                }
                ++i;
            }
        }
        return true;
    }
}
