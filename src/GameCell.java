import javax.swing.*;

public class GameCell extends JLabel {
    public int winIndex = -1;
    public int x = -1;
    public int y = -1;
    public GameCell(ImageIcon imageIcon, int y, int x) {
        super(imageIcon);
        this.y = y;
        this.x = x;
    }
    public void swapPos(GameCell cell){
        int tmpX = x;
        int tmpY = y;
        x = cell.x;
        y = cell.y;
        cell.x = tmpX;
        cell.y = tmpY;
    }
}
